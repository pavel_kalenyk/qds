import {combineReducers} from "redux";

import { reducer as formReducer } from "redux-form";

import notes from "./notes";
import note from "./note";

const reducers=combineReducers({
    form: formReducer,
    notes,
    note
});
export default reducers;