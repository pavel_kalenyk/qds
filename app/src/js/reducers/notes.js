import {GET_NOTES, GET_NOTES_FAILURE, GET_NOTES_SUCCESS} from "../constants/notes";

const initialState = {
    loading: false,
    loaded: false,
    data: []
};

export default function (state = initialState, action) {
    switch (action.type) {
    case GET_NOTES: {
        return {
            ...state,
            loading: true
        };
    }
    case GET_NOTES_SUCCESS: {
        return {
            ...state,
            loading: false,
            loaded: true,
            data: action.payload
        };
    }
    case GET_NOTES_FAILURE: {
        return {
            ...state,
            loading: false,
            loaded: false,
            data: []
        };
    }
    default: {
        return state;
    }
    }
}