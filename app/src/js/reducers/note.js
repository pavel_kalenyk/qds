import {GET_NOTE, GET_NOTE_FAILURE, GET_NOTE_SUCCESS} from "../constants/notes";

const initialState = {
    loading: false,
    loaded: false,
    data: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
    case GET_NOTE: {
        return {
            ...state,
            loading: true
        };
    }
    case GET_NOTE_SUCCESS: {
        return {
            ...state,
            loading: false,
            loaded: true,
            data: action.payload
        };
    }
    case GET_NOTE_FAILURE: {
        return {
            ...state,
            loading: false,
            loaded: false,
            data: {}
        };
    }
    default: {
        return state;
    }
    }
}