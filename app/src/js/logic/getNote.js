import {createLogic} from "redux-logic";
import {GET_NOTE,GET_NOTE_FAILURE,GET_NOTE_SUCCESS} from "../constants/notes";
import {getRequest} from "../requests/requests";

const getNoteLogic=createLogic({
    type:GET_NOTE,
    latest:true,
    process({action},dispatch,done){
        getRequest(`notes/${action.id}`)
            .then(res =>{
                dispatch({
                    type:GET_NOTE_SUCCESS,
                    payload:res
                });
                done();
            })
            .catch(() => {
                dispatch({
                    type:GET_NOTE_FAILURE,
                    error:true
                });
                done();
            });
    }
});

export default getNoteLogic;