import getNotes from "./getNotes";
import getNote from "./getNote";
import deleteNote from "./deleteNote";
import createNote from "./createNote";
import updateNote from "./updateNote";

const actions=[
    getNotes,
    deleteNote,
    createNote,
    updateNote,
    getNote
];
export default actions;