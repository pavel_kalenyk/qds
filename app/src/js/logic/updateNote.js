import {createLogic} from "redux-logic";
import {UPDATE_NOTE,UPDATE_NOTE_FAILURE,UPDATE_NOTE_SUCCESS,GET_NOTE} from "../constants/notes";
import {putRequest} from "../requests/requests";

const updateNoteLogic=createLogic({
    type:UPDATE_NOTE,
    latest:true,
    process({action},dispatch,done){
        putRequest(`notes/${action.id}`,action.data)
            .then(() =>{
                dispatch({
                    type:UPDATE_NOTE_SUCCESS,
                });
                dispatch({
                    type:GET_NOTE,
                    id:action.id
                });
                done();
            })
            .catch(() => {
                dispatch({
                    type:UPDATE_NOTE_FAILURE,
                    error:true
                });
                done();
            });
    }
});

export default updateNoteLogic;