import {createLogic} from "redux-logic";

import {history} from "../config/history";
import {DELETE_NOTE,DELETE_NOTE_FAILURE,DELETE_NOTE_SUCCESS} from "../constants/notes";
import {deleteRequest} from "../requests/requests";

const deleteNoteLogic=createLogic({
    type:DELETE_NOTE,
    latest:true,
    process({action},dispatch,done){
        deleteRequest(`notes/${action.id}`)
            .then(() =>{
                dispatch({
                    type:DELETE_NOTE_SUCCESS,
                });
                history.push({pathname:"/"});
                done();
            })
            .catch(() => {
                dispatch({
                    type:DELETE_NOTE_FAILURE,
                    error:true
                });
                done();
            });
    }
});

export default deleteNoteLogic;