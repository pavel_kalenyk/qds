import {createLogic} from "redux-logic";
import {GET_NOTES,GET_NOTES_FAILURE,GET_NOTES_SUCCESS} from "../constants/notes";
import {getRequest} from "../requests/requests";

const getNotesLogic=createLogic({
    type:GET_NOTES,
    latest:true,
    process(_,dispatch,done){
        getRequest("notes")
            .then(res =>{
                dispatch({
                    type:GET_NOTES_SUCCESS,
                    payload:res
                });
                done();
            })
            .catch(() => {
                dispatch({
                    type:GET_NOTES_FAILURE,
                    error:true
                });
                done();
            });
    }
});

export default getNotesLogic;