import {createLogic} from "redux-logic";
import {ADD_NOTE,ADD_NOTE_FAILURE,ADD_NOTE_SUCCESS,GET_NOTES} from "../constants/notes";
import {postRequest} from "../requests/requests";

const createNoteLogic=createLogic({
    type:ADD_NOTE,
    latest:true,
    process({action},dispatch,done){
        postRequest("note/new",{...action.data,isCompleted:action.data.isCompleted || false, date:Date.now()})
            .then(() =>{
                dispatch({
                    type:ADD_NOTE_SUCCESS,
                });
                dispatch({
                    type:GET_NOTES,
                });
                done();
            })
            .catch(() => {
                dispatch({
                    type:ADD_NOTE_FAILURE,
                    error:true
                });
                done();
            });
    }
});

export default createNoteLogic;