import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";

import {store} from "./config/store";
import {history} from "./config/history";
import "./requests/fetchIntercept";


import NotesListContainer from "./Components/NotesListContainer";
import NoteContainer from "./Components/NoteContainer";

ReactDOM.render(<Provider store={store}>
    <ConnectedRouter history={history}>
        <Switch>
            <Route exact path='/' component={NotesListContainer}/>
            <Route path="/notes/:id" component={NoteContainer}/>
        </Switch>
    </ConnectedRouter>
</Provider>
    ,document.getElementById("app"));