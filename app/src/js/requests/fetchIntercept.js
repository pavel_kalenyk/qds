import fetchIntercept from "fetch-intercept";
import {history} from "../config/history";

fetchIntercept.register({
    request:function (path,config) {
        config.mode="cors";

        config.headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        };
        return [path, config];
    },
    response: (response) => {
        return response;
    },
});