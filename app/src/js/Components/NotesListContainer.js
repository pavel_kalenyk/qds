import React from "react";
import {connect} from "react-redux";
import {reset} from 'redux-form';

import {getNotes,addNote,deleteNote} from "../actions/notes";

import styled from "styled-components";

import NoteItems from "./NoteItems";
import NoteForm from "./NoteForm";

const Wrapper = styled.div`
    display:flex;
    flex-direction:column;
    align-items:center;
    `;

const Button = styled.button`
    outline:none;
    border:none;
    width:150px
    cursor:pointer;
    margin-bottom:50px
    `;


class NotesListContainer extends React.Component{
    state = {
        showForm:false
    };
    componentDidMount(){
        this.props.getNotes()
    }
    render(){
        return(<Wrapper>
            <Button onClick={()=>this.setState(prevState=>({showForm:!prevState.showForm}))}>Add new note</Button>

                <NoteItems notes={this.props.notes} deleteNote={(id)=>{
                    this.props.deleteNote(id)}
                }/>


            <NoteForm show={this.state.showForm} onSubmit={(data)=>{
                this.setState(prevState=>({showForm:!prevState.showForm}));
                this.props.addNote(data);
                this.props.resetForm()}
            } />
        </Wrapper>)
    }
}

function mapStateToProps(state) {
    return{
        notes:state.notes.data
    }
}
function mapDispatchToProps(dispatch) {
    return{
        getNotes(){
            dispatch(getNotes())
        },
        addNote(data){
            dispatch(addNote(data))
        },
        deleteNote(id){
            dispatch(deleteNote(id))
        },
        resetForm(){
            dispatch(reset("NoteForm"))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(NotesListContainer);