import React from "react";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Delete from "material-ui/svg-icons/navigation/close";
import IconButton from "material-ui/IconButton";
import Edit from "material-ui/svg-icons/editor/border-color";
import Card from "material-ui/Card";
import CardActions from "material-ui/Card/CardActions";
import CardContent from "material-ui/Card/CardText";

export default class NoteCard extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <Card>
                    <CardContent>
                        Title: {this.props.note.title}<br/>
                        Text: {this.props.note.text}<br/>
                        Completed: {this.props.note.isCompleted? "Yes":"No"}

                    </CardContent>
                    <CardActions>
                        <IconButton><Delete onClick={this.props.deleteNote} color="red"/></IconButton>
                        <IconButton><Edit onClick={this.props.editNote} color="black"/></IconButton>
                    </CardActions>
                </Card>
            </MuiThemeProvider>
        );
    }
}