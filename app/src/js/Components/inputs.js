import React from "react";

import styled from "styled-components";

const Input = styled.input`
    margin: 5px 0 0 5px;
    outline:none;
    width:100%;
        border: 3px solid grey;

`;

const Textarea = styled.textarea`
    height:200px;
    margin: 5px 5px 20px 5px;
    padding:0;
    outline:none;
    width:100%;
    border: 3px solid grey;
`;

const Error = styled.div`
    margin-left:5px;
    font-size:10px;
    color:red;
    font-weight:bold;
`;

const Wrapper = styled.div`
    width:75%;
`;
export const renderField = ({input, label, type, meta: {touched, error}}) => (
    <Wrapper>
        <Input {...input} placeholder={label} type={type}/>
        {touched && (error && <Error>{error}</Error>)}
    </Wrapper>
);

export const renderTextareaField = ({input, label, type, meta: {touched, error}}) => (
    <Wrapper>
        <Textarea {...input} placeholder={label} />
        {touched && (error && <Error>{error}</Error>)}
    </Wrapper>
);
