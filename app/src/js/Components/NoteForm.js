import React from "react";
import {Field, reduxForm} from "redux-form";
import styled from "styled-components";

import { renderField, renderTextareaField} from "./inputs";
import {maxTextLength, maxTitleLength,required} from "../validation/note";

const Form = styled.form`
    display:flex;
    width:100%;
    justify-content:center;
    flex-direction:column;
    align-items:center;
    margin-top:20px;
`;

const Button = styled.button`
    background:green;
    border:none;
    outline:none;
    width:60px
    height: 30px;
    border-radius:5px;
    cursor:pointer;
    margin-right:10px
    font-weight:bold
`;
const ClearButton = Button.extend`
    background:darkred;
`;

const StyledCheckbox = styled(Field)`
    margin:10px 0 30;
    `;

class NoteForm extends React.Component{
    render(){
        return(
            <Form style={{display:this.props.show?"flex":"none"}} onSubmit={this.props.handleSubmit}>
                <Field name="title" type="text"
                    component={renderField} label="Title"
                    validate={[ required, maxTitleLength ]}
                />
                <Field name="text"
                    component={renderTextareaField} label="Text"
                    validate={[required,maxTextLength]}
                />

                <StyledCheckbox component="input" name="isCompleted"  type="checkbox" normalize={v => !!v}/>

                <div>
                    <Button type="submit" disabled={this.props.submitting}>Submit</Button>
                    <ClearButton disabled={this.props.pristine || this.props.submitting} onClick={this.props.reset}>Clear</ClearButton>
                </div>
            </Form>
        );
    }
}

export default reduxForm({form:"NoteForm"})(NoteForm);