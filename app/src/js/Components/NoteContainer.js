import React from "react";
import {connect} from "react-redux";
import {initialize} from "redux-form";

import {getNote,deleteNote,updateNote} from "../actions/notes";


import NoteCard from "./NoteCard";
import NoteForm from "./NoteForm";

class NoteContainer extends React.Component{
    state={
        showForm:false
    };
    componentDidMount(){
        this.props.getNote(this.props.match.params.id);
    }
    editNote = () => {
        this.props.initializeForm(this.props.note);
    };
    render(){

        return(
            <div>
                <NoteCard note={this.props.note} editNote={()=>{this.setState(prevState=>({showForm:!prevState.showForm})); this.editNote()}} deleteNote={()=>this.props.deleteNote(this.props.match.params.id)}/>

                    <NoteForm show={this.state.showForm} onSubmit={(data)=>{
                        this.setState(prevState=>({showForm:!prevState.showForm}));
                        this.props.updateNote(this.props.match.params.id,data)}}/>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return{
        note:state.note.data
    }
}
function mapDispatchToProps(dispatch) {
    return{
        getNote(id){
            dispatch(getNote(id))
        },
        deleteNote(id){
            dispatch(deleteNote(id))
        },
        initializeForm(data){
            dispatch(initialize("NoteForm",data))
        },
        updateNote(id,data){
            dispatch(updateNote(id,data))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(NoteContainer);