import React from "react";

import {Link} from "react-router-dom";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {GridList, GridTile} from "material-ui/GridList";
import styled from "styled-components";

const StyledListItem = styled(GridTile)`
    border:2px solid grey;
    background-color:lightgrey;
`;

const StyledLink = styled(Link)`
    color:black;
    text-decoration:none
`;

const Completed = styled.div`
    padding:5px;
    width:100%;
    border-radius:5px;
    background:green;
    margin-right:40px;
    text-align:center
`;

const NotCompleted = Completed.extend`
    background:red;
`;

export default class NoteItems extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <GridList
                    cellHeight={180}

                >
                    {this.props.notes.map((item) => {
                        let date=new Date(item.date);
                        date=date.toString();
                        date=date.slice(4,24);
                        return(
                            <StyledLink to={`notes/${item._id}`} key={item._id}>

                                <StyledListItem
                                    title={item.title}
                                    subtitle={date}
                                    actionIcon={item.isCompleted ? <Completed>Completed</Completed> : <NotCompleted>Not Completed</NotCompleted>}
                                >
                                    {item.text}
                                </StyledListItem>
                            </StyledLink>);
                    }
                    )}
                </GridList>
            </MuiThemeProvider>
        );
    }
}