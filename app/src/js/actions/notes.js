import { createAction } from "redux-actions";

import {GET_NOTES, DELETE_NOTE, ADD_NOTE, UPDATE_NOTE,GET_NOTE} from "../constants/notes";

export const getNotes = () => ({
    type:GET_NOTES
});

export const getNote = (id) => ({
    type:GET_NOTE,
    id
});


export const addNote = (data) => ({
    type:ADD_NOTE,
    data
});
export const deleteNote = (id) => ({
    type:DELETE_NOTE,
    id
});

export const updateNote = (id,data) => ({
    type:UPDATE_NOTE,
    id,
    data
});
