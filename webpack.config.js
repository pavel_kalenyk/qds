const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: ["./app/src/js/index.js"],
    output: {
        publicPath: "/",
        path: path.resolve(__dirname, "public"),
        filename: "js/[name].js"
    },
    devServer: {
        contentBase: "./public",
        historyApiFallback: true,

    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {loader: "babel-loader"}
            }
        ]
    },
    plugins:[
        new HtmlWebPackPlugin({
            template: "./app/public/index.html",
            filename: "./index.html"
        }),
        new ExtractTextPlugin('style.css')
    ]
};
