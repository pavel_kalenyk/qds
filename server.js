const express = require("express"),
    app = express(),
    port = 8000,
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    cors = require("cors");

require("./api/models/notesModel");


mongoose.Promise = global.Promise;

mongoose.connect("mongodb://admin:admin1@ds135186.mlab.com:35186/notes");
app.use(cors());


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


const routes = require("./api/routes/index");
routes(app);



app.use(function (req, res) {
    res.status(404).send({url: req.originalUrl + " not found"});
});


console.log("Application listen:  " + port);
app.listen(port);

