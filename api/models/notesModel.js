"use strict";

const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

let NotesSchema = new Schema({
    title: {
        type: String,
        Required: true
    },
    text: {
        type: String,
        required: true
    },
    isCompleted: {
        type: Boolean,
        required: true
    },
    date: {
        type: Number,
        required: true
    }
});


module.exports = mongoose.model("Notes", NotesSchema);