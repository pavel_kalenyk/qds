"use strict";

module.exports = function (app) {
    const notes = require("../controllers/notesController");

    app.route("/notes")
        .get(notes.getAllNotes)
        .delete(notes.deleteAllNotes);


    app.route("/notes/:id")
        .delete(notes.deleteNote)
        .put(notes.updateNote)
        .get(notes.getOneNote);

    app.route("/note/new")
        .post(notes.createNewNote);
};
