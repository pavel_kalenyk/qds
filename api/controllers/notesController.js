"use strict";

const mongoose = require("mongoose"),
    Notes = mongoose.model("Notes");


exports.getAllNotes = function (req, res) {
    Notes.find({}, function (err, note) {
        if (err)
            res.send(err);
        res.json(note);
    });
};


exports.createNewNote = function (req, res) {
    let note = new Notes(req.body);
    note.save(function (err, note) {
        if (err) {
            return   res.send(err);
        }
        return res.json({status:200});
    });
};

exports.updateNote = function (req, res) {

    Notes.update({"_id":req.params.id},req.body, function (err, note) {
        if (err) {
            return   res.send(err);
        }
        return res.json({status:200});
    });
};

exports.getOneNote = function (req, res) {
    Notes.findById(req.params.id, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};


exports.deleteNote = function (req, res) {

    Notes.remove({
        _id: req.params.id
    }, function (err, note) {
        if (err) {
            return res.json({status:404});
        }
        else {
            return res.json({status:200});
        }
    });

};

exports.deleteAllNotes = function (req, res) {

    Notes.remove({},function (err, db) {
        if (err) {
            return res.send({status:404});
        }
        else {
            return res.send({status:200});
        }
    });

};
